```bash
$ cd web/modules/custom/TrackPizzaStatus
$ php ws.php start

# optional (already builded)
$ yarn install
$ yarn build 
```

Admin section `/admin/config/track_pizza_status/manage` (can find menu in `Configuration`)

User section `/track_pizza_status`.
