import Vue from 'vue';
import CustomerOrders from './CustomerOrders'
import VueNativeSock from 'vue-native-websocket'

Vue.use(VueNativeSock, 'ws://localhost:2346', {
  connectManually: true,
})

window.onload = function () {
  const vm = new Vue({
    el: '#track-pizza-status-customer',
    render: h => h(CustomerOrders),
  });
};
