<?php
/**
 * @file
 * Contains \Drupal\track_pizza_status\Controller\SectionController.
 */

namespace Drupal\track_pizza_status\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\track_pizza_status\Form\OrderStatusChangeForm;

/**
 * Class SectionController
 *
 * @package Drupal\track_pizza_status\Controller
 */
class SectionController extends ControllerBase {

  const STATUS_CREATED = 0;

  const STATUS_MAKE = 1;

  const STATUS_DELIVERY = 2;

  const STATUS_COMPLETED = 3;

  /**
   * Customer section
   *
   * @return array
   */
  public function customer() {

    $blockManager = \Drupal::service('plugin.manager.block');
    $account = \Drupal::currentUser();
    $config = [
      'account' => $account,
      'orders' => $this->getOrders($account),
    ];
    /** @var \Drupal\track_pizza_status\Plugin\Block\StatusTrackerBlock $pluginBlock */
    $pluginBlock = $blockManager->createInstance('track_pizza_status_status_tracker', $config);
    $blockRender = $pluginBlock->build();
    return $blockRender;
  }

  /**
   * Admin section
   *
   * @return array
   */
  public function admin() {
    $rows = [];
    $rows[] = [
      ['data' => 'Order'],
      ['data' => 'Price'],
      ['data' => 'Customer'],
      ['data' => 'Created'],
      ['data' => 'Status'],
    ];
    foreach ($this->getOrders() as $order) {
      $form = \Drupal::formBuilder()->getForm(OrderStatusChangeForm::class);
      $form['order_id']['#value'] = $order->order_id;
      $form['customer_id']['#value'] = $order->customer_id;
      // IDK why default not set.
      $form['status']['#default_value'] = $order->status;
      $rows[] = [
        ['data' => sprintf('#%d', $order->order_id)],
        ['data' => sprintf('%.2f', $order->price)],
        ['data' => sprintf('%s', $order->name)],
        ['data' => sprintf('%s', $order->created)],
        [
          'data' => $form,
        ],
      ];
    }
    $content['table'] = [
      '#type' => 'table',
      '#rows' => $rows,
    ];
    return $content;
  }

  /**
   * @param \Drupal\Core\Session\AccountProxyInterface|NULL $account
   *
   * @return array
   */
  public function getOrders(AccountProxyInterface $account = NULL) {
    /** prepare query */
    $selectQuery = \Drupal::database()
      ->select('track_pizza_status', 'tps')
      ->fields('tps', [
        'order_id',
        'customer_id',
        'price',
        'status',
        'created',
        'changed',
      ]);;

    /** if account is NULL - get all orders with join account */
    if (NULL === $account) {
      /** check current user. If not admin - return only his orders */
      $currentUser = \Drupal::currentUser();
      $roles = $currentUser->getRoles();
      if (!in_array('administrator', $roles)) {
        return $this->getOrders($currentUser);
      }
      $selectQuery
        ->addJoin('left', 'users_field_data', 'ufd', 'ufd.uid = tps.customer_id');
      $selectQuery->fields('ufd', ['name']);
    }
    /** else get orders for specified user */
    else {
      $selectQuery
        ->condition('customer_id', $account->id());
    }

    $result = $selectQuery->execute()->fetchAll();
    return $result;
  }
}
