<?php

namespace Drupal\track_pizza_status\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Event that is fired when a status changed
 */
class StatusChangedEvent extends Event {

  const STATUS_CHANGED = 'track_pizza_status_changed';

  public $orderId;

  public $status;

  public $customerId;

  /**
   * Constructs the object.
   *
   * @param $orderId
   * @param $status
   * @param $customerId
   */
  public function __construct($orderId, $status, $customerId) {
    $this->orderId = $orderId;
    $this->status = $status;
    $this->customerId = $customerId;
  }

}
