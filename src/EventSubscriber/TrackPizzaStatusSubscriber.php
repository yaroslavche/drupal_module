<?php

namespace Drupal\track_pizza_status\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\track_pizza_status\Event\StatusChangedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Track Pizza Status event subscriber.
 */
class TrackPizzaStatusSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      StatusChangedEvent::STATUS_CHANGED => ['onStatusChanged'],
    ];
  }

  /**
   * @param \Drupal\track_pizza_status\Event\StatusChangedEvent $event
   */
  public function onStatusChanged(StatusChangedEvent $event) {
    $updateQueryResult = \Drupal::database()
      ->update('track_pizza_status')
      ->fields([
        'status' => $event->status,
      ])
      ->condition('order_id', $event->orderId)
      ->execute();

    $localsocket = 'tcp://127.0.0.1:1234';
    $instance = stream_socket_client($localsocket);
    fwrite($instance, json_encode(['order_id' => $event->orderId, 'status' => $event->status, 'customer_id' => $event->customerId])  . "\n");

    \Drupal::messenger()
      ->addMessage('Status successfully changed');
  }
}
