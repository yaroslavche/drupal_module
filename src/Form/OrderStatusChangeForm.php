<?php

namespace Drupal\track_pizza_status\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\track_pizza_status\Event\StatusChangedEvent;

/**
 * Provides a Track Pizza Status form.
 */
class OrderStatusChangeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'track_pizza_status_order_status_change';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['order_id'] = [
      '#type' => 'hidden',
    ];
    $form['customer_id'] = [
      '#type' => 'hidden',
    ];
    $form['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Select status'),
      '#options' => [
        '0' => $this->t('Created'),
        '1' => $this->t('Cooking'),
        '2' => $this->t('Delivery'),
        '3' => $this->t('Completed'),
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Change'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!in_array($form_state->getValue('status'), ['0', '1', '2', '3'])) {
      $form_state->setErrorByName('status', $this->t('Status is not correct.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $orderId = $form_state->getValue('order_id');
    $status = $form_state->getValue('status');
    $customerId = $form_state->getValue('customer_id');
    $event = new StatusChangedEvent($orderId, $status, $customerId);
    $eventDispatcher = \Drupal::service('event_dispatcher');
    $eventDispatcher->dispatch(StatusChangedEvent::STATUS_CHANGED, $event);
  }

}
