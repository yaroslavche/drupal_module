<?php

namespace Drupal\track_pizza_status\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Provides a status tracker block.
 *
 * @Block(
 *   id = "track_pizza_status_status_tracker",
 *   admin_label = @Translation("Status Tracker"),
 *   category = @Translation("Custom")
 * )
 */
class StatusTrackerBlock extends BlockBase {

  /** @var AccountProxyInterface $account */
  private $account;

  /** @var array $orders */
  private $orders;

  /**
   * StatusTrackerBlock constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->account = $configuration['account'];
    $this->orders = $configuration['orders'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#theme' => 'track_pizza_status_customer_block',
      '#account' => $this->account,
      '#orders' => json_encode($this->orders),
      '#attached' => [
        'library' => [
          'track_pizza_status/vue-track-pizza-status',
        ],
      ],
    ];
    return $build;
  }

}
