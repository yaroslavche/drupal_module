<?php
require_once __DIR__ . '/vendor/autoload.php';

use Workerman\Worker;

$users = [];

$ws_worker = new Worker("websocket://0.0.0.0:2346");
$ws_worker->onWorkerStart = function () use (&$users) {
  $inner_tcp_worker = new Worker("tcp://127.0.0.1:1234");
  $inner_tcp_worker->onMessage = function ($connection, $data) use (&$users) {
    $data = json_decode($data);
    if (isset($users[$data->customer_id])) {
      $webconnection = $users[$data->customer_id];
      $webconnection->send(json_encode([
        'order_id' => $data->order_id,
        'status' => $data->status,
      ]));
    }
  };
  $inner_tcp_worker->listen();
};

$ws_worker->onConnect = function ($connection) use (&$users) {
  $connection->onWebSocketConnect = function ($connection) use (&$users) {
    $users[$_GET['customer_id']] = $connection;
  };
};

$ws_worker->onClose = function ($connection) use (&$users) {
  $user = array_search($connection, $users);
  unset($users[$user]);
};

Worker::runAll();
